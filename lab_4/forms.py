from django import forms
from .models import Jadwal


class JadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        # CATEGORY_LIST = ['Class', 'Commitee', 'Organization', 'Eat', 'Drink']
        fields = ['category', 'activity', 'date', 'time', 'place']
        widgets = {
            # 'category' : forms.ChoiceField(choices = CATEGORY_LIST),
            'date': forms.DateInput(attrs={'type': 'date'}),
            'time': forms.TimeInput(attrs={'type': 'time'})
            }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })