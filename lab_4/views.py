from django.shortcuts import render, redirect
from .models import Jadwal
from . import forms

# Create your views here.
def home(request):
    return render(request, 'html.html')

def profile(request):
    return render(request, 'profile.html')

def works(request):
    return render(request, 'works.html')

def activities(request):
    return render(request, 'activities.html')

def education(request):
    return render(request, 'education.html')



def schedule(request):
    schedules = Jadwal.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    form = forms.JadwalForm(request.POST) 
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('schedule')

    else:
        form = forms.JadwalForm()
    return render(request, 'create_schedule.html', {'form': form})

def schedule_delete(request):
	Jadwal.objects.all().delete()
	return render(request, "schedule.html")

