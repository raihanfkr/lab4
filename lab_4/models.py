from django.db import models
from datetime import datetime


# Create your models here.
class Jadwal(models.Model):

    category = models.CharField(max_length = 20, default="")
    activity = models.CharField(max_length = 100, default="")
    date = models.DateTimeField()
    time = models.TimeField()
    place = models.CharField(max_length= 20, default="")

    def __str__(self):
        return self.activity